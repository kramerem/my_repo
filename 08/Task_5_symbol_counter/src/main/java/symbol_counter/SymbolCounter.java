package symbol_counter;

public class SymbolCounter {
    public static void main(String[] args) {
        String text = "I live in a house near the mountains. I have two brothers and one sister, and I was born last. " +
                "My father teaches mathematics, and my mother is a nurse at a big hospital.";

        System.out.println(getSymbolQuantity(text, 'd'));
    }

    public static int getSymbolQuantity(String text, char symbol) {
        int counter = 0;
        for (int i = 0; i < text.length(); i++) {
            if (text.charAt(i) == symbol) counter++;
        }
        return counter;
    }
}
