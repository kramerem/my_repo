package fibonacci;

import java.util.Arrays;

public class Fibonacci {
    public static void main(String[] args) {
        int[] fibonacciRow = getFibonacciRow(14); 
        Arrays.stream(fibonacciRow).forEach(v -> System.out.print(v + " ")); 
    }

    public static int[] getFibonacciRow(int number) { 
        int[] arr = new int[number];  
        arr[0] = 0;
        arr[1] = 1;
        for (int i = 2; i < arr.length; ++i) {
            arr[i] = arr[i - 1] + arr[i - 2];
        }
        return arr;
    }
}
