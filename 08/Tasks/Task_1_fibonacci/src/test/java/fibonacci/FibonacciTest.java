package fibonacci;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class FibonacciTest {
    @Test
    public void getFibonacciRow() {
        int[] array = new int[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        assertArrayEquals(array, Fibonacci.getFibonacciRow(10));
    }

    @Test
    public void getFibonacciRowRecursively() {
        long[] array = new long[]{0, 1, 1, 2, 3, 5, 8, 13, 21, 34};
        assertArrayEquals(array, RecursiveFibonacci.getFibonacciRowRecursively(10));
    }
}
